package com.demo.devops.leapyearcalculator;

public class FizzBuzzCalculator {

	public String GetString(int i) {
		if (IsDivideBy3(i) && IsDivideBy5(i)) {
			return "FizzBuzz";
		} else
		if (IsDivideBy3(i)) {
			return "fizz";
		} else if (IsDivideBy5(i)) {
				return "buzz";
		}
			else {
		}
			return Integer.toString(i);
		}
	
	public boolean IsDivideBy3(int i) {
		return i % 3 == 0;
	}
	
	public boolean IsDivideBy5(int i) {
		return i % 5 == 0;
	}
	}

